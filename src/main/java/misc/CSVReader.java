package misc;

import bTree.BTree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by AlvarBer on 10/01/16.
 */
public class CSVReader {

	public BTree read(String filepath) {
		BufferedReader br = null;
		String line;
		String cvsSplitBy = ",";
		BTree <String> tree = new BTree<String>();

		try {
			br = new BufferedReader(new FileReader(filepath));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] lineStr = line.split(cvsSplitBy);

				for (String word : lineStr)
					tree.insert(word);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return tree;
	}

}