package bTree;

public class BPair <KeyType> {
	// Attributes
	private BTree.BNode node;
	private int index;

	// Constructors
	public BPair(BTree.BNode node, int index) {
		this.node = node;
		this.index = index;
	}
}