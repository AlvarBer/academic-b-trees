/*package bTree;

import java.util.Vector;
import java.util.List;

public class BNodeOld <KeyType extends Comparable<KeyType>> {
	// Attributes
	private static final int t = BTree.t; // Minimum Degree
	private Vector<KeyType> keys; // We use a vector because it allows us to implement a "static array"
	private Vector<BNode> children; // but with size that can be established at runtime.
	private boolean leaf;

	// Constructors
	public BNode() { // Null Constructor
		instantiateKeys();
		instantiateChildren();
		leaf = true;
	}

	public BNode(KeyType key) { // Single key value constructor
		this();
		keys.add(0, key);
	}

	public BNode(List<KeyType> keyList) { // Multiple key constructor
		this();
		keys.addAll(keyList);
	}

	public BNode(List<KeyType> keyList, List<BNode> childrenList) { // Multiple key and child constructor
		this(keyList);
		children.addAll(childrenList);
		leaf = false;
	}

	public BNode(BNode child) { // Single child constructor
		this();
		children.add(child);
		leaf = false;
	}

	public BNode(BNode left, BNode right) { // Merge Constructor
		this(left.getKeys(),left.getChildren());
		keys.addAll(right.getKeys());
		children.addAll(right.getChildren());
		leaf = left.leaf;
	}

	// Accessors
	protected Vector<KeyType> getKeys() {
		return keys;
	}

	protected Vector<BNode> getChildren() {
		return children;
	}

	protected boolean isLeaf() {
		return leaf;
	}

	@SuppressWarnings("unchecked")
	protected void stealLeftKey(BNode leftNode) {
		keys.add(0, (KeyType) leftNode.keys.remove(leftNode.numberOfKeys() - 1));
		if (!leftNode.isLeaf())
			children.add(0, (BNode) leftNode.children.remove(leftNode.numberOfKeys()));
	}

	@SuppressWarnings("unchecked")
	protected void stealRightKey(BNode rightNode) {
		keys.add((KeyType) rightNode.keys.remove(0));
		if (!rightNode.isLeaf())
			children.add((BNode) rightNode.children.remove(0));
	}

	protected int numberOfKeys() {
		return keys.size();
	}

	// Public Methods
	protected boolean isFull() {
		return keys.size() == 2*t - 1;
	}

	protected void split(BNode fullChild) {
		if (!fullChild.isFull())
			System.err.print("Node you are trying to split is not full!!");

		BNode newChild;
		if (fullChild.isLeaf()) {
			newChild = new BNode(fullChild.getKeys().subList(t, 2 * t - 1));
		}
		else {
			newChild = new BNode(fullChild.getKeys().subList(t, 2 * t - 1),
					fullChild.getChildren().subList(t, 2 * t));
			fullChild.getChildren().subList(t, 2 * t).clear();
		}

		insert((KeyType) fullChild.getKeys().get(t - 1), newChild);
		fullChild.getKeys().subList(t - 1, 2 * t - 1).clear();
	}

	protected void remove(KeyType key) {
		if(!keys.remove(key))
			System.err.println("Element " + key + "couldn't be removed");
	}

	protected int contains(KeyType key) {
		if (keys.contains(key))
			return keys.indexOf(key);
		else
			return -1;
	}

	@Override
	public String toString() {
		String s = "";
		for (KeyType key : keys)
			s = s.concat(key + " ");

		return s;
	}

	// Private/Restricted methods
	protected void insert(KeyType key, BNode newChild) {
		int i = 0;

		while (i < numberOfKeys() && key.compareTo(keys.get(i)) > 0) {
			i++;
		}

		keys.add(i, key);
		if (newChild != null)
			children.add(i + 1, newChild);
	}

	private void instantiateKeys() {
		keys = new Vector<KeyType>(2*t - 1);
	}

	private void instantiateChildren() {
		children = new Vector<BNode>(2*t);
	}
}*/