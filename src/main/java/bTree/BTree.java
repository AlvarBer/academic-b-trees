package bTree;

import org.omg.CORBA.BAD_PARAM;

import java.util.List;
import java.util.Vector;


public class BTree <KeyType extends Comparable<KeyType>> {

	// Attributes
	private BNode root;
	protected static int t = 3;

	// Constructors
	public BTree() {
		root = new BNode();
	}

	// Public Methods
	public void insert(KeyType key) {
		if (root.isFull()) {
			BNode newParent = new BNode(root);
			root = newParent;
			newParent.split(root.children.get(0));

			nonFullInsert(newParent, key);
		}
		else
			nonFullInsert(root, key);
	}

	public void remove(KeyType key) {
		remove(root, key);
	}

	private void remove(BNode subTree, KeyType key) {
		if (subTree.isLeaf())
			subTree.remove(key);
		else {
			int ind = subTree.contains(key);
			if (ind != -1) {
				BNode predecessor = root.children.get(ind);
				BNode successor = root.children.get(ind + 1);
				if (predecessor.numberOfKeys() >= t) {
					KeyType pKey = predecessor.keys.get(predecessor.numberOfKeys() - 1);
					predecessor.remove(pKey);
					root.keys.set(ind, pKey);
				}
				else if (successor.numberOfKeys() >= t) {
					KeyType sKey = successor.keys.get(0);
					successor.remove(sKey);
					root.keys.set(ind, sKey);
				}
				else {
					BNode mergedNode = new BNode(predecessor, successor);
					root.children.set(ind, mergedNode);
					root.children.remove(ind + 1);
				}
			}
			else {
				int i = 0;
				while (i < subTree.numberOfKeys() && !lower(key, root.keys.get(i)))
					++i;

				BNode selectedChild = root.children.elementAt(i);
				if (selectedChild.numberOfKeys() == t - 1) {
					BNode littleSibling = null;
					BNode bigSibling = null;
					if (i > 0)
						littleSibling = root.children.elementAt(i - 1);
					if (i < root.children.size() - 1)
						bigSibling = root.children.elementAt(i + 1);

					if (littleSibling != null && littleSibling.numberOfKeys() >= t)
						selectedChild.stealLeftKey(littleSibling);
					else if (bigSibling != null && bigSibling.numberOfKeys() >= t)
						selectedChild.stealRightKey(bigSibling);
					else {
						int medianPosition = 0;
						KeyType keyPivot;
						if (littleSibling != null) {
							keyPivot = selectedChild.keys.elementAt(0);
							while (lower(keyPivot, root.keys.get(medianPosition))) {
								++medianPosition;
							}
							selectedChild.keys.add(0, root.keys.remove(medianPosition));
							selectedChild = new BNode(littleSibling, selectedChild);
							root.children.remove(root.children.indexOf(littleSibling));
							if (root.keys.isEmpty())
								root = selectedChild;
						}
						else if (bigSibling != null) {
							keyPivot = selectedChild.keys.elementAt(selectedChild.keys.size() - 1);
							while (!lower(keyPivot, root.keys.get(medianPosition))) {
								++medianPosition;
							}
							selectedChild.keys.add(root.keys.remove(medianPosition));
							selectedChild = new BNode(selectedChild, bigSibling);
							root.children.remove(root.children.indexOf(bigSibling));
							if (root.keys.isEmpty()) // Special Case when the root is empty
								root = selectedChild;
						}
						else
							System.err.println("No sibling to merge");
					}
				}

				remove(selectedChild, key);
			}
		}
	}

	public BPair search(KeyType key) {
		return search(root, key);
	}

	private BPair search(BNode subTree, KeyType key) {
		Vector<KeyType> v = subTree.getKeys();
		Vector<BNode> children = subTree.getChildren();
		int i = 0;
		while (i < subTree.numberOfKeys() && !lower(key, v.get(i)))
			++i;

		if (i < subTree.numberOfKeys() && equal(key, v.get(i)))
			return new BPair(subTree, i);
		if (!subTree.isLeaf())
			return search(children.get(i), key);
		else
			return null;
	}

	@Override
	public String toString() {
		String s = "";
		DequeQueue dualQueue[] = new DequeQueue[2];
		dualQueue[0] = new DequeQueue();
		dualQueue[1] = new DequeQueue();

		dualQueue[0].add(root);
		s = printTree(dualQueue, 0, s);

		return s;
	}

	// Private auxiliary methods
	private void nonFullInsert(BNode node, KeyType key) {
		if (node.isLeaf()) { // Insert here
			node.insert(key, null);
		}
		else {
			Vector<KeyType> keys = node.getKeys();
			int n = node.numberOfKeys() - 1;
			while (n >= 0 && lower(key, keys.get(n)))
				--n;

			if (n >= 0 && equal(key,keys.get(n)))
				System.err.println("Tried to insert an element that was already in the tree");

			++n;
			BNode child = (BNode) node.getChildren().get(n);
			if (child.isFull()) {
				node.split(child);
				if (equal(key,keys.get(n)))
					++n;
			}
			nonFullInsert((BNode) node.getChildren().get(n), key);
		}
	}

	private String printTree(DequeQueue[] dualQueue, int even, String s) {
		int odd = even ^ 1;

		s = s.concat("| ");
		while (!dualQueue[even].isEmpty()) {
			BNode node = dualQueue[even].remove();
			Vector<BNode> children = node.getChildren();
			s = s.concat(node.toString());
			if (children != null) {
				for (BNode child : children)
					dualQueue[odd].add(child);
			}
			s = s.concat("| ");
		}
		s = s.concat("\n");
		even ^= 1;
		if (!dualQueue[odd].isEmpty())
			s = printTree(dualQueue, even, s);

		return s;
	}

	// Comparison functions Using Comparable instead of KeyType to avoid casts
	private static boolean lower(Comparable k1, Comparable k2) {
		return k1.compareTo(k2) < 0;
	}

	private static boolean equal(Comparable k1, Comparable k2) {
		return k1.compareTo(k2) < 0;
	}

	public class BNode {
		// Attributes
		private Vector<KeyType> keys; // We use a vector because it allows us to implement a "static array"
		private Vector<BNode> children; // but with size that can be established at runtime.
		private boolean leaf;

		// Constructors
		public BNode() { // Null Constructor
			instantiateKeys();
			instantiateChildren();
			leaf = true;
		}

		public BNode(KeyType key) { // Single key value constructor
			this();
			keys.add(0, key);
		}

		public BNode(List<KeyType> keyList) { // Multiple key constructor
			this();
			keys.addAll(keyList);
		}

		public BNode(List<KeyType> keyList, List<BNode> childrenList) { // Multiple key and child constructor
			this(keyList);
			children.addAll(childrenList);
			leaf = false;
		}

		public BNode(BNode child) { // Single child constructor
			this();
			children.add(child);
			leaf = false;
		}

		public BNode(BNode left, BNode right) { // Merge Constructor
			this(left.getKeys(),left.getChildren());
			keys.addAll(right.getKeys());
			children.addAll(right.getChildren());
			leaf = left.leaf;
		}

		// Accessors
		protected Vector<KeyType> getKeys() {
			return keys;
		}

		protected Vector<BNode> getChildren() {
			return children;
		}

		protected boolean isLeaf() {
			return leaf;
		}

		@SuppressWarnings("unchecked")
		protected void stealLeftKey(BNode leftNode) {
			keys.add(0, leftNode.keys.remove(leftNode.numberOfKeys() - 1));
			if (!leftNode.isLeaf())
				children.add(0, leftNode.children.remove(leftNode.numberOfKeys()));
		}

		@SuppressWarnings("unchecked")
		protected void stealRightKey(BNode rightNode) {
			keys.add((KeyType) rightNode.keys.remove(0));
			if (!rightNode.isLeaf())
				children.add((BNode) rightNode.children.remove(0));
		}

		protected int numberOfKeys() {
			return keys.size();
		}

		// Public Methods
		protected boolean isFull() {
			return keys.size() == 2*t - 1;
		}

		protected void split(BNode fullChild) {
			if (!fullChild.isFull())
				System.err.print("Node you are trying to split is not full!!");

			BNode newChild;
			if (fullChild.isLeaf()) {
				newChild = new BNode(fullChild.getKeys().subList(t, 2 * t - 1));
			}
			else {
				newChild = new BNode(fullChild.getKeys().subList(t, 2 * t - 1),
						fullChild.getChildren().subList(t, 2 * t));
				fullChild.getChildren().subList(t, 2 * t).clear();
			}

			insert((KeyType) fullChild.getKeys().get(t - 1), newChild);
			fullChild.getKeys().subList(t - 1, 2 * t - 1).clear();
		}

		protected void remove(KeyType key) {
			if(!keys.remove(key))
				System.err.println("Element " + key + " couldn't be removed");
		}

		protected int contains(KeyType key) {
			if (keys.contains(key))
				return keys.indexOf(key);
			else
				return -1;
		}

		@Override
		public String toString() {
			String s = "";
			for (KeyType key : keys)
				s = s.concat(key + " ");

			return s;
		}

		// Private/Restricted methods
		protected void insert(KeyType key, BNode newChild) {
			int i = 0;

			while (i < numberOfKeys() && key.compareTo(keys.get(i)) > 0) {
				i++;
			}

			keys.add(i, key);
			if (newChild != null)
				children.add(i + 1, newChild);
		}

		private void instantiateKeys() {
			keys = new Vector<KeyType>(2*t - 1);
		}

		private void instantiateChildren() {
			children = new Vector<BNode>(2*t);
		}
	}
}