package bTree;

class NodeNotFullException extends Exception {
	public NodeNotFullException() {
    }

    public NodeNotFullException(String message) {
        super(message);
    }

    public NodeNotFullException(Throwable cause) {
        super(cause);
    }

    public NodeNotFullException(String message, Throwable cause) {
        super(message, cause);
    }
}