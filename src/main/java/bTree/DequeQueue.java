package bTree;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by AlvarBer on 9/01/16.
 *
 */
public class DequeQueue {
	private Deque<BTree.BNode> deque;

	/**
	 * Constructor using ArrayDeque
	 *
	 */
	public DequeQueue() {
		this.deque = new ArrayDeque<BTree.BNode>();
	}

	/**
	 * We push a Move into the stack
	 *
	 * @param child The node we want to add to the queue
	 */
	public void add(BTree.BNode child) {
		deque.add(child);
	}

	/**
	 * We pop the last move in the Stack
	 *
	 * @return Returns the head of the queue
	 */
	public BTree.BNode remove() {
		return deque.removeFirst();
	}

	/**
	 * We check if the Dequeue is empty
	 *
	 * @return True if empty, false otherwise
	 */
	public boolean isEmpty() {
		return deque.isEmpty();
	}
}
