import bTree.BTree;
import misc.CSVReader;

public class Main {

	public static void main(String[] args) {
		CSVReader reader = new CSVReader();

		BTree<String> corDelTree;
		corDelTree = reader.read("inputCormenDelete.csv");
		System.out.println(corDelTree);

		corDelTree.remove("Z");
		corDelTree.remove("P");
		corDelTree.remove("U");
		corDelTree.remove("N");
		corDelTree.remove("D");
		System.out.println(corDelTree);
		corDelTree.remove("J");
		System.out.println(corDelTree);

		BTree<Integer> intTree;
		intTree = reader.read("inputIntegers.csv");
		System.out.println(intTree);

		BTree<String> strTree;
		strTree = reader.read("inputStrings.csv");
		System.out.println(strTree);


		if (strTree.search("impelled") == null)
			System.out.println("We couldn't find element impelled");
		else
			System.out.println("We found element impelled");

	}
}