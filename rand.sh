#!/bin/bash

STRFILENAME=inputStrings.csv
INTFILENAME=inputIntegers.csv
NUMOFRANDS=50;
COUNT=1;

rm STRFILENAME INTFILENAME 2> /dev/null

while test $COUNT -lt $NUMOFRANDS ; do 
	echo "$(shuf -n1 /usr/share/dict/words)," >> $STRFILENAME
	echo "$RANDOM," >> $INTFILENAME
	let "COUNT += 1"
done
